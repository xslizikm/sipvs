﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template match="/invoice">
		<html>
			<head>
				<meta charset="utf-8"/>
				<title>Your bill</title> 
				<style>
					.invoice-box{
						max-width:800px;
						margin:auto;
						padding:30px;
						border:1px solid #eee;
						box-shadow:0 0 10px rgba(0, 0, 0, .15);
						font-size:16px;
						line-height:24px;
						font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
						color:#555;
					}
    
					.invoice-box table{
						width:100%;
						line-height:inherit;
						text-align:left;
					}
    
					.invoice-box table td{
						padding:5px;
						vertical-align:top;
					}
    
					.invoice-box table tr td:nth-child(2){
						text-align:right;
					}
	
					.invoice-box table tr td:nth-child(3){
						text-align:right;
					}
    
					.invoice-box table tr.top table td{
						padding-bottom:20px;
					}
    
					.invoice-box table tr.top table td.title{
						font-size:45px;
						line-height:45px;
						color:#333;
					}
    
					.invoice-box table tr.information table td{
						padding-bottom:40px;
					}
    
					.invoice-box table tr.heading td{
						background:#eee;
						border-bottom:1px solid #ddd;
						font-weight:bold;
					}
    
					.invoice-box table tr.details td{
						padding-bottom:20px;
					}
    
					.invoice-box table tr.item td{
						border-bottom:1px solid #eee;
					}
    
					.invoice-box table tr.item.last td{
						border-bottom:none;
					}
    
					.invoice-box table tr.total td:nth-child(3){
						border-top:2px solid #eee;
						font-weight:bold;
					}
    
					@media only screen and (max-width: 600px) {
						.invoice-box table tr.top table td{
							width:100%;
							display:block;
							text-align:center;
						}
        
						.invoice-box table tr.information table td{
							width:100%;
							display:block;
							text-align:center;
						}
					}
				</style>
   			</head>
			<body>
				<div class="invoice-box">
					<table cellpadding="0" cellspacing="0">
						<tr class="top">
							<td colspan="3">
								<table>
									<tr>
										<td><h2>Invoice</h2></td>
										
										<td>Created: <xsl:value-of select="createdAt"/><br/>
										</td>
										
									</tr>
								</table>
							</td>
						</tr>
            
            <tr class="information">
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
								
                                <xsl:value-of select="recipient"/><br/>
                                <xsl:value-of select="recipientAddress"/><br/>
								ID: <xsl:value-of select="recipientIdentifier"/>
                            </td>
							<td></td>
                            <td>
                                <xsl:value-of select="client"/><br/>
								<xsl:value-of select="address"/><br/>
								ID: <xsl:value-of select="identifier"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>Name</td>
				<td>Category info</td>
                <td>Price</td>
            </tr>
            
			<xsl:apply-templates select="items"/>
			
            			
            <tr class="total">
                <td><h4>Sum price</h4></td>
                <td></td>
                <td>
                   <xsl:value-of select="finalPrice"/> €
                </td>
            </tr>
        </table>
    </div>
	</body>
		</html>
    </xsl:template>
	
<xsl:template match="/items">
    <xsl:apply-templates select="item"/>
</xsl:template>
	
<xsl:template match="item">
  <tr class="item">
        <td>
            <xsl:value-of select="name"/>
        </td>
		<td>
			<xsl:value-of select="@hours"/>
		</td>
        <td>
            <xsl:value-of select="price"/> €
        </td>
    </tr>
</xsl:template>


</xsl:stylesheet>
