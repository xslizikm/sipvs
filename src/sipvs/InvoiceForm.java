package sipvs;

import javax.swing.*;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.xml.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ProcessingInstruction;
import org.xml.sax.SAXException;
@SuppressWarnings("serial")




public class InvoiceForm extends JFrame implements ActionListener{
		
	//paths to xsd,xsl and transformed html
	String schemaPath = "invoice.xsd";
	String xslPath = "invoice.xsl";
	String clientName = "Yoga 4 You";
	String clientAddress = "Homolova 7, 841 01 Bratislava";
	String clientIdentifier = "5021521";
	String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
	int cnt = 0;
	int y = 330;

	JLabel  clientLabel2, IDLabel2, addressLabel2, totalLabel,title, title2, hoursLabel1, clientLabel, addressLabel, IDLabel, dateLabel, contactLabel1, priceLabel1;
	JTextField clientField2, addressField2, IDField2, dateField2, totalField,hoursField1, clientField, addressField, IDField, itemField1, priceField1;             
	JButton validateButton, saveButton, showButton, plusButton;         
    JLabel[] contactLabels = new JLabel[6];
    JLabel[] hoursLabels = new JLabel[6];
    JLabel[] priceLabels = new JLabel[6];
    JTextField[] contactFields = new JTextField[6];
    JTextField[] hoursFields = new JTextField[6];
    JTextField[] priceFields = new JTextField[6];

    

    private JLabel[] createContactLabels(){
        JLabel[] labels=new JLabel[6];
        for (int i=0;i<6;i++){
            labels[i]=new JLabel("Name");
        }
        return labels;
    }
    
    private JLabel[] createPriceLabels(){
        JLabel[] labels=new JLabel[6];
        for (int i=0;i<6;i++){
            labels[i]=new JLabel("Price");
        }
        return labels;
    }
    
    private JLabel[] createHoursLabels(){
        JLabel[] labels=new JLabel[6];
        for (int i=0;i<6;i++){
            labels[i]=new JLabel("Nr. of hours");
        }
        return labels;
    }
    
    private JTextField[] createContactFields(){
        JTextField[] fields=new JTextField[6];
        for (int i=0;i<6;i++){
            fields[i]=new JTextField();
        }
        return fields;
    }
    
    private JTextField[] createHoursFields(){
        JTextField[] fields=new JTextField[6];
        for (int i=0;i<6;i++){
            fields[i]=new JTextField();
        }
        return fields;
    }
    
    private JTextField[] createPriceFields(){
        JTextField[] fields=new JTextField[6];
        for (int i=0;i<6;i++){
            fields[i]=new JTextField();
        }
        return fields;
    }
    
InvoiceForm() {                               
	setSize(900, 750);                               
	setLayout(null);
	title = new JLabel("Invoice");                             
	title.setBounds(60, 25, 200, 30);
	Font labelFont = title.getFont();
	title.setFont(new Font(labelFont.getName(), Font.BOLD, 21));
	title2 = new JLabel("Items: (fill at least 1 item)");                             
	title2.setBounds(60, 240, 200, 30);
	
	clientLabel = new JLabel("Recipient*");
	clientLabel.setBounds(30,115, 90, 30);
	IDLabel = new JLabel("Identifier*");
	IDLabel.setBounds(30,205, 90, 30);
	addressLabel = new JLabel("Address*");                              
	addressLabel.setBounds(30, 160, 90, 30);
	
	clientLabel2 = new JLabel("Client");
	clientLabel2.setBounds(500,115, 90, 30);
	IDLabel2 = new JLabel("Identifier");
	IDLabel2.setBounds(500,205, 90, 30);
	addressLabel2 = new JLabel("Address");                              
	addressLabel2.setBounds(500, 160, 90, 30);
	
	dateLabel = new JLabel("createdAt:"); 
	dateLabel.setBounds(600, 10, 90, 30);
	
	contactLabel1 = new JLabel("Name*");                               
	contactLabel1.setBounds(30, 285, 60, 30);
	priceLabel1 = new JLabel("Price*");                               
	priceLabel1.setBounds(690, 285, 60, 30);	                     
	
	totalLabel = new JLabel("Sum price"); 
	totalLabel.setBounds(30, 590, 120, 30);
	
	
	hoursLabel1 = new JLabel("Nr. of hours");                               
	hoursLabel1.setBounds(440, 285, 90, 30);	

	
	clientField = new JTextField(); 
    clientField.setBounds(125, 115, 200, 30);                             
    addressField = new JTextField();  //prijimatel
	addressField.setBounds(125, 160, 200, 30);
	IDField = new JTextField();   
	IDField.setBounds(125, 205, 200, 30);
	
	clientField2 = new JTextField(); 
    clientField2.setBounds(600, 115, 200, 30);                             
    addressField2 = new JTextField();  //prijimatel
	addressField2.setBounds(600, 160, 200, 30);
	IDField2 = new JTextField();   
	IDField2.setBounds(600, 205, 200, 30);
	
	clientField2.setEditable(false);
	addressField2.setEditable(false);
	IDField2.setEditable(false);
	
	clientField2.setText(clientName);
	addressField2.setText(clientAddress);
	IDField2.setText(clientIdentifier);
	IDField.addKeyListener(new KeyAdapter() { 
	    
	public void keyTyped(KeyEvent e) {                             
			char c = e.getKeyChar();
	                                                         
			if(!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {                                                                           
				e.consume();                                               
			}                                              
		}                              
	});
	
	dateField2 = new JTextField();
	dateField2.setBounds(700,10,150, 30);
	dateField2.setEditable(false);
	dateField2.setText(date);

	
	priceField1 = new JTextField(); 
	priceField1.setBounds(750, 285, 90, 30);
	priceField1.addKeyListener(new KeyAdapter() { 
	public void keyTyped(KeyEvent e) {                             
			char c = e.getKeyChar();
		                                                         
			if(!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {                                                                           
				e.consume();                                               
			}                                              
		}                              
	});
	
	itemField1 = new JTextField();                
	itemField1.setBounds(90, 285, 330, 30);                    


	hoursField1 = new JTextField();                
	hoursField1.setBounds(520, 285, 150, 30);

	saveButton = new JButton("Save"); 
	saveButton.setBounds(230, 650, 100, 30);
	saveButton.addActionListener(this);	
	validateButton = new JButton("Validate");
	validateButton.setBounds(360, 650, 100, 30);
	validateButton.addActionListener(this);
	showButton = new JButton("Show"); 
	showButton.setBounds(490, 650, 100, 30);
	showButton.addActionListener(this);
	plusButton = new JButton("+");
	plusButton.setBounds(620,650,50,30);
	plusButton.addActionListener(this);
	

	totalField = new JTextField();                
	totalField.setBounds(750, 590, 90, 30);
	totalField.setEditable(false);
	
	contactLabels=createContactLabels();
    for (int i=0;i<contactLabels.length;i++)
        this.add(contactLabels[i]);
     
    hoursLabels=createHoursLabels();
    for (int i=0;i<hoursLabels.length;i++)
        this.add(hoursLabels[i]);
    
    priceLabels=createPriceLabels();
    for (int i=0;i<priceLabels.length;i++)
        this.add(priceLabels[i]);
    
    contactFields=createContactFields();
    for (int i=0;i<contactFields.length;i++)
        this.add(contactFields[i]);
    
    hoursFields=createHoursFields();
    for (int i=0;i<hoursFields.length;i++)
        this.add(hoursFields[i]);
    
    priceFields=createPriceFields();
    for (int i=0;i<priceFields.length;i++)
        this.add(priceFields[i]);
	
	add(title);
	add(title2);
	add(clientLabel);
	add(IDLabel);
	add(clientLabel2);
	add(IDLabel2);
	add(dateLabel);  
	add(addressLabel);
	add(addressLabel2);
	add(priceLabel1);
	add(contactLabel1);
	add(clientField);
	add(addressField);
	add(IDField);
	add(clientField2);
	add(addressField2);
	add(IDField2);
	add(dateField2);
	add(priceField1);
	add(itemField1);                          
	add(hoursField1);
	add(hoursLabel1);
	add(saveButton);
	add(showButton);
	add(validateButton);
	add(plusButton);
	add(totalField);         
	add(totalLabel);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	setResizable(true); 
	setVisible(true);
	
}


@Override
public void actionPerformed(ActionEvent ae) {
	
	
	if(ae.getSource() == saveButton) {
		                    
		if(clientField.getText().equals("") ||
		addressField.getText().equals("") ||
		IDField.getText().equals(""))
		
			JOptionPane.showMessageDialog(clientField,  "Please fill all fields"); 

		else {
			boolean a = true;
			try {
	
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				// root elements
				Document doc = docBuilder.newDocument();
				doc.setXmlStandalone(true);
				ProcessingInstruction pi = doc.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"my.stylesheet.xsl\"");
				
				Element rootElement = doc.createElement("invoice");
				doc.appendChild(rootElement);
				doc.insertBefore(pi, rootElement);
				
				Element client = doc.createElement("client");
				client.appendChild(doc.createTextNode(clientName));
				
				Element identifier = doc.createElement("identifier");
				identifier.appendChild(doc.createTextNode(clientIdentifier));
				
				Element address = doc.createElement("address");
				address.appendChild(doc.createTextNode(clientAddress));
				
				Element createdAt = doc.createElement("createdAt");
				
				createdAt.appendChild(doc.createTextNode(date));
				
			
				Element recipient = doc.createElement("recipient");
				recipient.appendChild(doc.createTextNode(clientField.getText()));
				Element recipientAddress = doc.createElement("recipientAddress");
				recipientAddress.appendChild(doc.createTextNode(addressField.getText()));
				Element ico = doc.createElement("recipientIdentifier");
				ico.appendChild(doc.createTextNode(IDField.getText()));
				
				rootElement.appendChild(client);
				rootElement.appendChild(address);
				rootElement.appendChild(identifier);
				rootElement.appendChild(createdAt);
				rootElement.appendChild(recipient);
				rootElement.appendChild(recipientAddress);
				rootElement.appendChild(ico);
				
				Element items = doc.createElement("items");
				rootElement.appendChild(items);
				
				double totalPrice = 0;
				if (!(itemField1.getText().equals(""))) {
				Element item = doc.createElement("item");
				item.setAttribute("hours", hoursField1.getText());
				Element name = doc.createElement("name");
				name.appendChild(doc.createTextNode(itemField1.getText()));
				Element price = doc.createElement("price");
				price.appendChild(doc.createTextNode(priceField1.getText()));
				item.appendChild(name);
				item.appendChild(price);
				totalPrice = totalPrice + Double.parseDouble(priceField1.getText());
				items.appendChild(item);
				}
				
				for (int i = 0 ; i<6;i++)
				{
					if (!(contactFields[i].getText().equals(""))) {
					Element item2 = doc.createElement("item");
					item2.setAttribute("hours", hoursFields[i].getText());
					Element name2 = doc.createElement("name");
					name2.appendChild(doc.createTextNode(contactFields[i].getText()));
					Element price2 = doc.createElement("price");
					price2.appendChild(doc.createTextNode(priceFields[i].getText()));				
					item2.appendChild(name2);
					item2.appendChild(price2);
					totalPrice = totalPrice + Double.parseDouble(priceFields[i].getText());
					items.appendChild(item2);
					}
					else
						break;
				}
				

				Element total = doc.createElement("finalPrice");
				total.appendChild(doc.createTextNode(Double.toString(totalPrice)));
				rootElement.appendChild(total);
	
				JFrame parentFrame = new JFrame();
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Specify a file to save");
				int userSelection = fileChooser.showSaveDialog(parentFrame);
				if (userSelection == JFileChooser.APPROVE_OPTION) {
					 try {
				           
				            File fileToSave = fileChooser.getSelectedFile();
				            String resultPath=fileToSave.getAbsolutePath();
				            TransformerFactory transformerFactory = TransformerFactory.newInstance();
							Transformer transformer = transformerFactory.newTransformer();
							DOMSource source = new DOMSource(doc);
							StreamResult result = new StreamResult(new File(resultPath));
							transformer.transform(source, result);
				            System.out.println("Save as file: " + fileToSave.getAbsolutePath());
				        } catch (Exception ex) {
				            ex.printStackTrace();

				        }
				}
				
				totalField.setText(Double.toString(totalPrice));
				System.out.println("File saved!");
				
			  } catch (ParserConfigurationException pce) {
				  a = false;
				pce.printStackTrace();
				JOptionPane.showMessageDialog(saveButton,  "Error when saving!");
			  } catch (NumberFormatException nfee) {
				  nfee.printStackTrace();
				  a = false;
				  JOptionPane.showMessageDialog(saveButton,  "Error when saving!");
		  }
			if (a)
				JOptionPane.showMessageDialog(saveButton,  "Saved successfully!");
		}
		

	}
	
	if(ae.getSource() == validateButton) {
		boolean a = true;
		try {
			JFileChooser fileChooser = new JFileChooser();
			int result = fileChooser.showOpenDialog(this);
			if (result == JFileChooser.APPROVE_OPTION) {
			    File selectedFile = fileChooser.getSelectedFile();
			    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
			   
			    // parse an XML document into a DOM tree
			    DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			    Document document = parser.parse(new File(selectedFile.toString()));

			    // create a SchemaFactory capable of understanding WXS schemas
			    SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			    // load a WXS schema, represented by a Schema instance
			    Source schemaFile = new StreamSource(new File(schemaPath));
			    Schema schema = factory.newSchema(schemaFile);

			    // create a Validator instance, which can be used to validate an instance document
			    Validator validator = schema.newValidator();

			    // validate the DOM tree
			    validator.validate(new DOMSource(document));
			}
		
	  
	    } catch (SAXException e) {
	    
	    	JOptionPane.showMessageDialog(validateButton,  "Error! Validation unsuccessful! \n" + "Error description: "+e.getMessage().split(":")[1]);
	    	a = false;
	        e.printStackTrace();
	    } catch (IOException e) {
	    	a = false;
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			a = false;
			e.printStackTrace();
		}
	    if (a)
	    	JOptionPane.showMessageDialog(validateButton,  "Validation successful!");
	}
	
	if (ae.getSource() == showButton) {
		try {

			    TransformerFactory tFactory = TransformerFactory.newInstance();
			    Transformer transformer =
			      tFactory.newTransformer
			         (new javax.xml.transform.stream.StreamSource
			            (xslPath));
				JFileChooser fileChooser = new JFileChooser();			
				int result = fileChooser.showOpenDialog(this);
				if (result == JFileChooser.APPROVE_OPTION) {
				    File selectedFile = fileChooser.getSelectedFile();
				    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
				   String pathToFile = selectedFile.getAbsolutePath().split("\\.")[0]+".html"; 
			    transformer.transform
			      (new javax.xml.transform.stream.StreamSource
			            (selectedFile.getAbsolutePath()),
			       new javax.xml.transform.stream.StreamResult
			            ( new FileOutputStream(pathToFile)));
			   
			    File file = new File(pathToFile);
		        Desktop.getDesktop().browse(file.toURI());
		       
				}
			 
				}
		  catch (Exception e) {
		    e.printStackTrace( );
		  }
	}
	
	if (ae.getSource() == plusButton) {
		try {
			if(cnt<6)
			{
			contactLabels[cnt].setBounds(30, y, 60, 30);
			contactLabels[cnt].setVisible(true);
			hoursLabels[cnt].setBounds(440, y, 90, 30);
			hoursLabels[cnt].setVisible(true);
			priceLabels[cnt].setBounds(690, y, 60, 30);
			priceLabels[cnt].setVisible(true);
			contactFields[cnt].setBounds(90, y, 330, 30);
			hoursFields[cnt].setBounds(520, y, 150, 30);
			priceFields[cnt].setBounds(750, y, 90, 30);

			y+=45;
			cnt++;
			}
		}
		  catch (Exception e) {
			    e.printStackTrace( );
			  }
	}
} 


	public static void main(String[] args) { 

		new InvoiceForm(); 
	}
}